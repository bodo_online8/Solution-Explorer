﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="19008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Test Files" Type="Folder">
			<Item Name="Test Classes" Type="Folder">
				<Item Name="Test Build Step.lvclass" Type="LVClass" URL="../../Test Files/Test Build Item/Test Build Step.lvclass"/>
				<Item Name="Test Solution Item.lvclass" Type="LVClass" URL="../../Test Files/Test Solution Item/Test Solution Item.lvclass"/>
				<Item Name="Tree.lvclass" Type="LVClass" URL="../../Test Files/Tree/Tree.lvclass"/>
			</Item>
			<Item Name="Test UI.vi" Type="VI" URL="../../Test Files/Test UI.vi"/>
		</Item>
		<Item Name="Project.lvlib" Type="Library" URL="../Project/Project.lvlib"/>
		<Item Name="Run VI.lvlib" Type="Library" URL="../Run VI/Run VI.lvlib"/>
		<Item Name="Shell Command.lvlib" Type="Library" URL="../Shell Command/Shell Command.lvlib"/>
		<Item Name="Solution.lvlibp" Type="LVLibp" URL="../../builds/PPLs/Solution.lvlibp">
			<Item Name="Interfaces" Type="Folder">
				<Item Name="Visualization.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Visualization/Visualization.lvclass"/>
				<Item Name="Build Artifact.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Build Artifact/Build Artifact.lvclass"/>
				<Item Name="Build Step.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Build Item/Build Step.lvclass"/>
				<Item Name="Item.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Item/Item.lvclass"/>
			</Item>
			<Item Name="Helpers" Type="Folder">
				<Item Name="Private" Type="Folder">
					<Item Name="Registry" Type="Folder">
						<Item Name="Error Handler" Type="Folder">
							<Item Name="Support" Type="Folder">
								<Item Name="AIT - Error Handler - Clear Error (Single).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/Error Handler/Support/AIT - Error Handler - Clear Error (Single).vi"/>
							</Item>
							<Item Name="AIT - Error Handler - Build Error Cluster.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/Error Handler/AIT - Error Handler - Build Error Cluster.vi"/>
						</Item>
						<Item Name="WinAPI" Type="Folder">
							<Item Name="Module" Type="Folder">
								<Item Name="AIT - WinAPI - GetModuleHandle.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Module/AIT - WinAPI - GetModuleHandle.vi"/>
							</Item>
							<Item Name="OS" Type="Folder">
								<Item Name="Support" Type="Folder">
									<Item Name="AIT - WinAPI - Convert OSVERSIONINFO cluster to OSVERSIONINFOEX cluster.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/OS/Support/AIT - WinAPI - Convert OSVERSIONINFO cluster to OSVERSIONINFOEX cluster.vi"/>
									<Item Name="AIT - WinAPI - OSVERSIONINFO.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/OS/Support/AIT - WinAPI - OSVERSIONINFO.ctl"/>
									<Item Name="AIT - WinAPI - OSVERSIONINFOEX.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/OS/Support/AIT - WinAPI - OSVERSIONINFOEX.ctl"/>
								</Item>
								<Item Name="AIT - WinAPI - Get OS Info (Basic).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/OS/AIT - WinAPI - Get OS Info (Basic).vi"/>
								<Item Name="AIT - WinAPI - Get System Information.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/OS/AIT - WinAPI - Get System Information.vi"/>
								<Item Name="AIT - WinAPI - Get Windows Version.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/OS/AIT - WinAPI - Get Windows Version.vi"/>
								<Item Name="AIT - WinAPI - GetVersionEx (Win9x).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/OS/AIT - WinAPI - GetVersionEx (Win9x).vi"/>
								<Item Name="AIT - WinAPI - GetVersionEx.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/OS/AIT - WinAPI - GetVersionEx.vi"/>
							</Item>
							<Item Name="Process" Type="Folder">
								<Item Name="AIT - WinAPI - Check if Running as WOW 64.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Process/AIT - WinAPI - Check if Running as WOW 64.vi"/>
								<Item Name="AIT - WinAPI - GetCurrentProcess.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Process/AIT - WinAPI - GetCurrentProcess.vi"/>
								<Item Name="AIT - WinAPI - GetProcessAddress.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Process/AIT - WinAPI - GetProcessAddress.vi"/>
								<Item Name="AIT - WinAPI - IsWow64Process.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Process/AIT - WinAPI - IsWow64Process.vi"/>
							</Item>
							<Item Name="Registry" Type="Folder">
								<Item Name="Support" Type="Folder">
									<Item Name="AIT - WinAPI - Registry Version.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Registry/Support/AIT - WinAPI - Registry Version.ctl"/>
								</Item>
								<Item Name="AIT - WinAPI - Open Registry Key (WOW64 Support).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Registry/AIT - WinAPI - Open Registry Key (WOW64 Support).vi"/>
							</Item>
							<Item Name="Support" Type="Folder">
								<Item Name="Constants" Type="Folder">
									<Item Name="Support" Type="Folder">
										<Item Name="AIT - WinAPI - Error (Enum to Error Code).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Support/Constants/Support/AIT - WinAPI - Error (Enum to Error Code).vi"/>
										<Item Name="AIT - WinAPI - Error (Error Code to Enum).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Support/Constants/Support/AIT - WinAPI - Error (Error Code to Enum).vi"/>
									</Item>
									<Item Name="AIT - WinAPI - Error Message.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Support/Constants/AIT - WinAPI - Error Message.vi"/>
									<Item Name="AIT - WinAPI - ERROR.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Support/Constants/AIT - WinAPI - ERROR.ctl"/>
									<Item Name="AIT - WinAPI - Error.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Support/Constants/AIT - WinAPI - Error.vi"/>
									<Item Name="AIT - WinAPI - OS.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Support/Constants/AIT - WinAPI - OS.ctl"/>
								</Item>
								<Item Name="AIT - WinAPI - API Error (Get Last Error).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Support/AIT - WinAPI - API Error (Get Last Error).vi"/>
								<Item Name="AIT - WinAPI - API Error (No Get Last Error).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Support/AIT - WinAPI - API Error (No Get Last Error).vi"/>
								<Item Name="AIT - WINAPI - Decode OSVERSIONINFO Cluster.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Support/AIT - WINAPI - Decode OSVERSIONINFO Cluster.vi"/>
								<Item Name="AIT - WINAPI - Decode OSVERSIONINFOEX Cluster.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Support/AIT - WINAPI - Decode OSVERSIONINFOEX Cluster.vi"/>
								<Item Name="AIT - WinAPI - OEM_ID.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Support/AIT - WinAPI - OEM_ID.ctl"/>
								<Item Name="AIT - WinAPI - PROCESSOR_ARCHITECTURE.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Support/AIT - WinAPI - PROCESSOR_ARCHITECTURE.ctl"/>
								<Item Name="AIT - WinAPI - Product Type (Pre-Vista).ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Support/AIT - WinAPI - Product Type (Pre-Vista).ctl"/>
								<Item Name="AIT - WinAPI - SYSTEM_INFO.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/Support/AIT - WinAPI - SYSTEM_INFO.ctl"/>
							</Item>
							<Item Name="AIT - WinAPI - API Error.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/AIT - WinAPI - API Error.vi"/>
							<Item Name="AIT - WinAPI - GetLastError.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Registry/WinAPI/AIT - WinAPI - GetLastError.vi"/>
						</Item>
					</Item>
					<Item Name="Sub Solution Build Step.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Solution/Build Sub Solution/Sub Solution Build Step.lvclass"/>
					<Item Name="Global.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Test Files/Tree/Global.vi"/>
					<Item Name="Unknown Item.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Unknown Item/Unknown Item.lvclass"/>
					<Item Name="Lookup LabVIEW Info.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Item/Lookup LabVIEW Info.vi"/>
					<Item Name="Sub Solution Item.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Sub Solution/Sub Solution Item.lvclass"/>
					<Item Name="Get Application Instance.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Item/Get Application Instance.vi"/>
					<Item Name="To Camel Case.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/To Camel Case.vi"/>
				</Item>
				<Item Name="Build Cache.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Build Cache/Build Cache.lvclass"/>
				<Item Name="Progress.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Progress/Progress.lvclass"/>
				<Item Name="Build Aborter.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Build Aborter/Build Aborter.lvclass"/>
				<Item Name="XML Node.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/XML Reader/XML Node.lvclass"/>
				<Item Name="Installed Solution Items.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Installed Solution Items.vi"/>
				<Item Name="Node.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Tree Entry/Node.lvclass"/>
				<Item Name="Set Delegates.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Visualization/Set Delegates.vi"/>
				<Item Name="Solution Icon.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Solution Icon.vi"/>
				<Item Name="Version.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Version.ctl"/>
			</Item>
			<Item Name="Solution.lvclass" Type="LVClass" URL="../../builds/PPLs/Solution.lvlibp/Solution/Solution/Solution.lvclass"/>
			<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
			<Item Name="NI_LVConfig.lvlib" Type="Library" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/config.llb/NI_LVConfig.lvlib"/>
			<Item Name="NI_FileType.lvlib" Type="Library" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/lvfile.llb/NI_FileType.lvlib"/>
			<Item Name="VariantType.lvlib" Type="Library" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/VariantDataType/VariantType.lvlib"/>
			<Item Name="NI_Data Type.lvlib" Type="Library" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/Data Type/NI_Data Type.lvlib"/>
			<Item Name="NI_XML.lvlib" Type="Library" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/xml/NI_XML.lvlib"/>
			<Item Name="MGI Natural Sort.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/1D Array/MGI Natural Sort.vi"/>
			<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Error Cluster From Error Code.vi"/>
			<Item Name="Get File Extension.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Get File Extension.vi"/>
			<Item Name="imagedata.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/picture/picture.llb/imagedata.ctl"/>
			<Item Name="Registry SAM.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Registry SAM.ctl"/>
			<Item Name="Registry refnum.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Registry refnum.ctl"/>
			<Item Name="Registry RtKey.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Registry RtKey.ctl"/>
			<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Registry WinErr-LVErr.vi"/>
			<Item Name="Registry Handle Master.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/Registry Handle Master.vi"/>
			<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/registry/registry.llb/STR_ASCII-Unicode.vi"/>
			<Item Name="LVRowAndColumnTypeDef.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/miscctls.llb/LVRowAndColumnTypeDef.ctl"/>
			<Item Name="FileVersionInformation.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
			<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
			<Item Name="BuildErrorSource.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
			<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
			<Item Name="GetFileVersionInfo.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
			<Item Name="VerQueryValue.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
			<Item Name="MoveMemory.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
			<Item Name="FileVersionInfo.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
			<Item Name="Get LV Class Default Value.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Default Value.vi"/>
			<Item Name="Load Plugin.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/Solution/Load Plugin.vi"/>
			<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
			<Item Name="Is Path and Not Empty.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/file.llb/Is Path and Not Empty.vi"/>
			<Item Name="MD5Checksum core.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/MD5Checksum.llb/MD5Checksum core.vi"/>
			<Item Name="MD5Checksum pad.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/MD5Checksum.llb/MD5Checksum pad.vi"/>
			<Item Name="MD5Checksum format message-digest.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/MD5Checksum.llb/MD5Checksum format message-digest.vi"/>
			<Item Name="MD5Checksum File.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/MD5Checksum.llb/MD5Checksum File.vi"/>
			<Item Name="GetNamedSemaphorePrefix.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/GetNamedSemaphorePrefix.vi"/>
			<Item Name="AddNamedSemaphorePrefix.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/AddNamedSemaphorePrefix.vi"/>
			<Item Name="Validate Semaphore Size.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/Validate Semaphore Size.vi"/>
			<Item Name="Semaphore Refnum Core.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/Semaphore Refnum Core.ctl"/>
			<Item Name="Semaphore RefNum" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/Semaphore RefNum"/>
			<Item Name="Obtain Semaphore Reference.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/Obtain Semaphore Reference.vi"/>
			<Item Name="Acquire Semaphore.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/Acquire Semaphore.vi"/>
			<Item Name="Not A Semaphore.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/Not A Semaphore.vi"/>
			<Item Name="Release Semaphore.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/Release Semaphore.vi"/>
			<Item Name="RemoveNamedSemaphorePrefix.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/RemoveNamedSemaphorePrefix.vi"/>
			<Item Name="Release Semaphore Reference.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/semaphor.llb/Release Semaphore Reference.vi"/>
			<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/express/express shared/ex_CorrectErrorChain.vi"/>
			<Item Name="subFile Dialog.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
			<Item Name="System Exec.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Platform/system.llb/System Exec.vi"/>
			<Item Name="MGI Open Explorer Window.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/File/MGI Open Explorer Window.vi"/>
			<Item Name="Clear Errors.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Clear Errors.vi"/>
			<Item Name="Compare Two Paths.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Compare Two Paths.vi"/>
			<Item Name="LVNumericRepresentation.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/numeric/LVNumericRepresentation.ctl"/>
			<Item Name=".NET Object To Variant.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Platform/dotnet.llb/.NET Object To Variant.vi"/>
			<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
			<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
			<Item Name="MGI Create Directory Chain Behavior Enum.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/File/MGI Create Directory Chain Behavior Enum.ctl"/>
			<Item Name="MGI Create Directory Chain.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/File/MGI Create Directory Chain.vi"/>
			<Item Name="MGI Suppress Error Code (Scalar).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Error Handling/MGI Suppress Error Code/MGI Suppress Error Code (Scalar).vi"/>
			<Item Name="MGI RWA Options Cluster.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Options Cluster.ctl"/>
			<Item Name="MGI RWA Write Strings to File.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Write Strings to File.vi"/>
			<Item Name="MGI RWA Read Strings from File.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Read Strings from File.vi"/>
			<Item Name="MGI Scan From String (CDB).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI Scan From String/MGI Scan From String (CDB).vi"/>
			<Item Name="MGI Scan From String (CSG).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI Scan From String/MGI Scan From String (CSG).vi"/>
			<Item Name="MGI Scan From String (CXT).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI Scan From String/MGI Scan From String (CXT).vi"/>
			<Item Name="MGI Scan From String (CXT[]).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI Scan From String/MGI Scan From String (CXT[]).vi"/>
			<Item Name="MGI Scan From String (CSG[]).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI Scan From String/MGI Scan From String (CSG[]).vi"/>
			<Item Name="MGI Scan From String (CDB[]).vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI Scan From String/MGI Scan From String (CDB[]).vi"/>
			<Item Name="MGI RWA Build Array Name.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Build Array Name.vi"/>
			<Item Name="MGI Insert Reserved Error.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Error Handling/MGI Insert Reserved Error.vi"/>
			<Item Name="MGI RWA Remove EOLs and Slashes.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Remove EOLs and Slashes.vi"/>
			<Item Name="MGI Windows Regional Ring.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI Windows Get Regional String/MGI Windows Regional Ring.ctl"/>
			<Item Name="MGI Windows Get Regional String.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI Windows Get Regional String.vi"/>
			<Item Name="MGI RWA Unreplace Characters.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Unreplace Characters.vi"/>
			<Item Name="MGI Get Cluster Elements.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Cluster/MGI Get Cluster Elements.vi"/>
			<Item Name="MGI RWA Convertion Direction Enum.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Convertion Direction Enum.ctl"/>
			<Item Name="MGI RWA Tag Lookup Cluster.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Tag Lookup Cluster.ctl"/>
			<Item Name="DWDT Empty Digital.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Waveform/DWDTOps.llb/DWDT Empty Digital.vi"/>
			<Item Name="DTbl Empty Digital.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Waveform/DTblOps.llb/DTbl Empty Digital.vi"/>
			<Item Name="MGI RWA Get Type Info.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Get Type Info.vi"/>
			<Item Name="MGI RWA Replace Characters.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Replace Characters.vi"/>
			<Item Name="Space Constant.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/dlg_ctls.llb/Space Constant.vi"/>
			<Item Name="MGI RWA Build Line.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Build Line.vi"/>
			<Item Name="MGI U8 Data to Hex Str.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI U8 Data to Hex Str.vi"/>
			<Item Name="whitespace.ctl" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/error.llb/whitespace.ctl"/>
			<Item Name="Trim Whitespace.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/vi.lib/Utility/error.llb/Trim Whitespace.vi"/>
			<Item Name="MGI RWA Enque Top Level Data.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Enque Top Level Data.vi"/>
			<Item Name="MGI Hex Str to U8 Data.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/String/MGI Hex Str to U8 Data.vi"/>
			<Item Name="MGI RWA Handle Tag or Refnum.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Handle Tag or Refnum.vi"/>
			<Item Name="MGI RWA Process Array Elements.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Process Array Elements.vi"/>
			<Item Name="MGI RWA Anything to String.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Anything to String.vi"/>
			<Item Name="MGI Write Anything.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI Write Anything.vi"/>
			<Item Name="MGI RWA INI Tag Lookup.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA INI Tag Lookup.vi"/>
			<Item Name="MGI RWA Unprocess Array Elements.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Unprocess Array Elements.vi"/>
			<Item Name="MGI RWA String To Anything.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA String To Anything.vi"/>
			<Item Name="MGI Read Anything.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI Read Anything.vi"/>
			<Item Name="MGI RWA Delete Section.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/Read Write Anything/MGI RWA Delete Section.vi"/>
			<Item Name="MGI Replace File Extension.vi" Type="VI" URL="../../builds/PPLs/Solution.lvlibp/1abvi3w/user.lib/_MGI/File/MGI Replace File Extension.vi"/>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="user.lib" Type="Folder">
				<Item Name="File Exists - Array__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/File Exists - Array__ogtk.vi"/>
				<Item Name="File Exists - Scalar__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/File Exists - Scalar__ogtk.vi"/>
				<Item Name="File Exists__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/file/file.llb/File Exists__ogtk.vi"/>
				<Item Name="MGI Caller&apos;s VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Caller&apos;s VI Reference.vi"/>
				<Item Name="MGI Clear Error.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Error Handling/MGI Clear Error.vi"/>
				<Item Name="MGI Current VI&apos;s Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Current VI&apos;s Reference.vi"/>
				<Item Name="MGI Get Tree Tag Children.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Tree/MGI Get Tree Tag Children.vi"/>
				<Item Name="MGI Gray if Empty String.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Gray if/MGI Gray if Empty String.vi"/>
				<Item Name="MGI Gray if Error.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Gray if/MGI Gray if Error.vi"/>
				<Item Name="MGI Gray if False.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Gray if/MGI Gray if False.vi"/>
				<Item Name="MGI Gray if True.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Gray if/MGI Gray if True.vi"/>
				<Item Name="MGI Gray if Zero.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Gray if/MGI Gray if Zero.vi"/>
				<Item Name="MGI Gray if(PolyVI).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Gray if(PolyVI).vi"/>
				<Item Name="MGI Is Runtime.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Is Runtime.vi"/>
				<Item Name="MGI Level&apos;s VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Level&apos;s VI Reference.vi"/>
				<Item Name="MGI Open Explorer Window.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/File/MGI Open Explorer Window.vi"/>
				<Item Name="MGI Origin at Top Left.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI Origin at Top Left.vi"/>
				<Item Name="MGI Replace File Extension.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/File/MGI Replace File Extension.vi"/>
				<Item Name="MGI Suppress Error Code (Array).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Error Handling/MGI Suppress Error Code/MGI Suppress Error Code (Array).vi"/>
				<Item Name="MGI Suppress Error Code (Scalar).vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Error Handling/MGI Suppress Error Code/MGI Suppress Error Code (Scalar).vi"/>
				<Item Name="MGI Suppress Error Code.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Error Handling/MGI Suppress Error Code.vi"/>
				<Item Name="MGI Top Level VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference/MGI Top Level VI Reference.vi"/>
				<Item Name="MGI True if Odd.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Numeric/Comparison/MGI True if Odd.vi"/>
				<Item Name="MGI VI Reference.vi" Type="VI" URL="/&lt;userlib&gt;/_MGI/Application Control/MGI VI Reference.vi"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="AB_Generate_Error_Cluster.vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/AB_Generate_Error_Cluster.vi"/>
				<Item Name="AB_Relative_Path_Type.ctl" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/AB_Relative_Path_Type.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="GetTargetBuildSpecs (project reference).vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/GetTargetBuildSpecs (project reference).vi"/>
				<Item Name="GetTargetBuildSpecs.vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/GetTargetBuildSpecs.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Invoke BuildTarget.vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/Invoke BuildTarget.vi"/>
				<Item Name="Is Name Multiplatform.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Is Name Multiplatform.vi"/>
				<Item Name="Is Path and Not Empty.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Is Path and Not Empty.vi"/>
				<Item Name="List Directory and LLBs.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/List Directory and LLBs.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="LVPositionTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPositionTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_App_Builder_API.lvlib" Type="Library" URL="/&lt;vilib&gt;/AppBuilder/AB_API_Simple/NI_App_Builder_API.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_XML.lvlib" Type="Library" URL="/&lt;vilib&gt;/xml/NI_XML.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Random Number (Range) DBL.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range) DBL.vi"/>
				<Item Name="Random Number (Range) I64.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range) I64.vi"/>
				<Item Name="Random Number (Range) U64.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range) U64.vi"/>
				<Item Name="Random Number (Range).vi" Type="VI" URL="/&lt;vilib&gt;/numeric/Random Number (Range).vi"/>
				<Item Name="Recursive File List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Recursive File List.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="sub_Random U32.vi" Type="VI" URL="/&lt;vilib&gt;/numeric/sub_Random U32.vi"/>
				<Item Name="subDisplayMessage.vi" Type="VI" URL="/&lt;vilib&gt;/express/express output/DisplayMessageBlock.llb/subDisplayMessage.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="AB_UI_Change_Path_from_Label.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/Builds/AppBuilder/AB_UI_Change_Path_from_Label.vi"/>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="DOMUserDefRef.dll" Type="Document" URL="DOMUserDefRef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="mscorlib" Type="VI" URL="mscorlib">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="provcom_CheckForInvalidCharacters.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/Common/provcom_CheckForInvalidCharacters.vi"/>
			<Item Name="provcom_GetTargetOS.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/Common/provcom_GetTargetOS.vi"/>
			<Item Name="provcom_StringGlobals.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/Common/provcom_StringGlobals.vi"/>
			<Item Name="provcom_Utility_IsEmptyOrWhiteSpace.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/Common/provcom_Utility_IsEmptyOrWhiteSpace.vi"/>
			<Item Name="System" Type="VI" URL="System">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="user32" Type="VI" URL="user32">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="user32.dll" Type="Document" URL="user32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Project" Type="Packed Library">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{F7F4EDC3-5DDF-4938-AF30-B4FE9CB5C689}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Project</Property>
				<Property Name="Bld_excludeDependentPPLs" Type="Bool">true</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/PPLs</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{7CB84EE7-8C3A-4567-8994-D1566783F271}</Property>
				<Property Name="Bld_version.build" Type="Int">6</Property>
				<Property Name="Bld_version.major" Type="Int">2</Property>
				<Property Name="Bld_version.minor" Type="Int">2</Property>
				<Property Name="Bld_version.patch" Type="Int">3</Property>
				<Property Name="Destination[0].destName" Type="Str">Project.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/PPLs/Project.lvlibp</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/PPLs</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{56FB06EA-CA7B-4231-809A-7046EE47566D}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Project.lvlib</Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[1].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[1].preventRename" Type="Bool">true</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Project</Property>
				<Property Name="TgtF_internalName" Type="Str">Project</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2016 </Property>
				<Property Name="TgtF_productName" Type="Str">Project</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{0E07FE6A-35AE-4FF3-8BA2-3AB670BC6439}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Project.lvlibp</Property>
			</Item>
			<Item Name="Run VI" Type="Packed Library">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{349EA32D-E692-4FE6-B389-1074FAF14DFC}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Run VI</Property>
				<Property Name="Bld_excludeDependentPPLs" Type="Bool">true</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/PPLs</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{06A2BEEE-4E50-40E8-8CB8-34EB7F874674}</Property>
				<Property Name="Bld_version.build" Type="Int">2</Property>
				<Property Name="Bld_version.major" Type="Int">2</Property>
				<Property Name="Bld_version.minor" Type="Int">2</Property>
				<Property Name="Bld_version.patch" Type="Int">3</Property>
				<Property Name="Destination[0].destName" Type="Str">Run VI.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/PPLs/Run VI.lvlibp</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/PPLs</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{56FB06EA-CA7B-4231-809A-7046EE47566D}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Project.lvlib</Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Run VI.lvlib</Property>
				<Property Name="Source[2].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[2].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[2].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[2].preventRename" Type="Bool">true</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[2].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">3</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Project</Property>
				<Property Name="TgtF_internalName" Type="Str">Project</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2016 </Property>
				<Property Name="TgtF_productName" Type="Str">Project</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{EC93FF99-092D-459C-AAC7-7D8446577267}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Run VI.lvlibp</Property>
			</Item>
			<Item Name="Shell Command" Type="Packed Library">
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{70F9AD45-CC9C-44AD-B988-12628FA6A8A9}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Shell Command</Property>
				<Property Name="Bld_excludeDependentPPLs" Type="Bool">true</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/PPLs</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{E14805A3-2644-4B90-A342-BF63378A4C23}</Property>
				<Property Name="Bld_version.build" Type="Int">2</Property>
				<Property Name="Bld_version.major" Type="Int">2</Property>
				<Property Name="Bld_version.minor" Type="Int">2</Property>
				<Property Name="Bld_version.patch" Type="Int">3</Property>
				<Property Name="Destination[0].destName" Type="Str">Shell Command.lvlibp</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/PPLs/Shell Command.lvlibp</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/PPLs</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="PackedLib_callersAdapt" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{56FB06EA-CA7B-4231-809A-7046EE47566D}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Project.lvlib</Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Run VI.lvlib</Property>
				<Property Name="Source[2].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[2].type" Type="Str">Library</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Shell Command.lvlib</Property>
				<Property Name="Source[3].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[3].Library.atomicCopy" Type="Bool">true</Property>
				<Property Name="Source[3].Library.LVLIBPtopLevel" Type="Bool">true</Property>
				<Property Name="Source[3].preventRename" Type="Bool">true</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[3].type" Type="Str">Library</Property>
				<Property Name="SourceCount" Type="Int">4</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Project</Property>
				<Property Name="TgtF_internalName" Type="Str">Project</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2016 </Property>
				<Property Name="TgtF_productName" Type="Str">Project</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{69522632-4320-4752-B46B-C0EDA3FD186E}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Shell Command.lvlibp</Property>
			</Item>
		</Item>
	</Item>
</Project>
