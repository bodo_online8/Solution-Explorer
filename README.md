# MGI Solution Explorer

The MGI Solution Explorer is a tool on the LabVIEW Tools Network to help developers manage complex, multi-project builds in LabVIEW.

Read technical article about the tool on [MGI Website](https://www.mooregoodideas.com/products/solution-explorer/index.html)

## Distribution and Installation

MGI Solution Explorer is distributed and installed using NI Package manager. A NIPM feed is being maintained so updating can be a breeze. Simply register the appropriate feed in your NIPM installation to receive the updates. [Instructions on registering a feed in NIPM.](http://www.ni.com/documentation/en/ni-package-manager/latest/manual/install-packages-from-feed/)

#### Distribution Links

| Installer                                                                                                    | NIPM Feed                                                |
| ------------------------------------------------------------------------------------------------------------ | -------------------------------------------------------- |
| [Latest Release](https://sln-exp-dist.s3-us-west-1.amazonaws.com/master/MGI+Solution+Explorer+Installer.exe) | https://sln-exp-dist.s3-us-west-1.amazonaws.com/master/  |
| [Latest Beta](https://sln-exp-dist.s3-us-west-1.amazonaws.com/develop/MGI+Solution+Explorer+Installer.exe)   | https://sln-exp-dist.s3-us-west-1.amazonaws.com/develop/ |

### Contribution guidelines

- All code should be written in **LabVIEW 2019**
- Pull requests should be small and concise.

### Questions and Comments

Email [support@mooregoodideas.com](mailto:support@mooregoodideas.com) for comments.
Also consider using this repository's [Issue Tracker](https://gitlab.com/mgi/Solution-Explorer/issues) to submit a bug report or feature request.
